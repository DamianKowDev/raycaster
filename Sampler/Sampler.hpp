#pragma once

#include <vector>
#include "../Utility/Vector2.hpp"
class Sampler
{
public:
    Sampler(int numSamples);
    virtual void GenerateSamples() = 0;
    void SetupShuffledIndices();
    void ShuffleSamples();
    Vector2 SampleUnitSquare();

    void SetNumSamples(int numSamples);
    void SetNumSets(int numSets);

    unsigned long GetCount();
    int GetNumSamples();


protected:
    int numSamples;
    int numSets;
    std::vector<Vector2> samples;
    std::vector<int> shuffledIndices;
    unsigned long _count;
    int jump;


};
