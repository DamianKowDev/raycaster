#include "Jittered.hpp"
#include <cmath>
#include "../Utility/Vector2.hpp"
#include <random>
#include <iostream>
#include "../Utility/Helper.hpp"

Jittered::Jittered(int numSamples)
    : Sampler(numSamples)
    {
       // std::cout << "Jittered constructor" << std::endl;
        GenerateSamples();
    }

void Jittered::GenerateSamples()
{
    //std::cout << "Jittered numSamples = " << this->numSamples << std::endl;
    int n = (int) sqrt(this->numSamples);
   // std::cout << "Jittered n = " << n << std::endl;
    this->numSets = 1;
  //  std::cout << "Jittered numSets = " << this->numSets << std::endl;
    int i = 0;

    for(int p = 0; p < numSets; p++)
    {
        for(int j = 0; j < n; j++)
        {
            for(int k = 0; k < n; k++)
            {
              //  std::cout << "Probka nr " << i++ << std::endl;
                Vector2 sp((k + RandomFloat(0.1, 0.2)) / n, (j + RandomFloat(0.1,0.2)) / n);
                this->samples.push_back(sp);
            }
        }
    }
    std::cout << "Probki wygenerowane " << this->samples.size() << std::endl;
}
