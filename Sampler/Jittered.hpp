#pragma once

#include "Sampler.hpp"

class Jittered : public Sampler
{
public:
    Jittered(int numSamples);
private:
    virtual void GenerateSamples();

};
