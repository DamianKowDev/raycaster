#include "Plane.hpp"
#include <limits>
#include <iostream>
#include <math.h>

Plane::Plane()
    :GeometricObject(RGBColor::RED)
{
    this->position = Vector3::zero;
    this->normal = Vector3::zero;
}

Plane::Plane(Vector3 &pos, Vector3 &normal)
    :GeometricObject(RGBColor::RED)
{
    this->position = pos;
    this->normal = normal;
}

Plane::Plane(Vector3 &pos, Vector3 &normal, RGBColor color)
    :GeometricObject(color)
{
    this->position = pos;
    this->normal = normal;
}

Plane::~Plane(){}

bool Plane::Hit( Ray ray, double& distance, ShadeRec shadeRec) const
{
    float t = (this->position - ray.origin).Dot(this->normal) / (ray.direction.Dot(this->normal));

   // std::cout << "t = " << t << std::endl;
   // if((std::isnan(t)))
   //     std::cout << "NAN " << ray.origin.x << " " << ray.origin.y;

    double kEpsilon = 0.000001;

    if( t > kEpsilon )
    {
        distance = t;
        shadeRec.normal = this->normal;
        shadeRec.localHitPoint = ray.origin + (ray.direction * t);
        shadeRec.hitAnObject = true;
        return true;
    }
    else
    {
        shadeRec.hitAnObject = false;
        return false;
    }
}
