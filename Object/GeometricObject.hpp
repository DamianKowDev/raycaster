#pragma once
#ifndef _GEOMETRICOBJECT_
#define _GEOMETRICOBJECT_

#include "../Utility/RGBColor.hpp"
#include "../Renderer/Ray.hpp"
#include "../Utility/ShadeRec.hpp"

class ShadeRec;

class GeometricObject
{
public:
    GeometricObject(RGBColor color);
    virtual bool Hit(Ray ray, double& distance, ShadeRec shadeRec) const = 0;
    void SetColor(RGBColor color);
    RGBColor GetColor();
protected:
    RGBColor color;
};




#endif // _GEOMETRICOBJECT_
