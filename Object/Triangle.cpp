#include "Triangle.hpp"
#include <iostream>

Triangle::Triangle()
: GeometricObject(RGBColor::RED),
a(0,0,0), b(0,0,1), c(1,0,0),
normal(0,1,0)
{}

Triangle::Triangle(const Vector3& v0, const Vector3& v1, const Vector3& v2)
: GeometricObject(RGBColor::RED),
a(v0), b(v1), c(v2)
{
    Vector3 ba = b - a;
    Vector3 ca = c - a;
    normal = ba.Cross(ca);
    normal = normal.Normalize();
}

Triangle::Triangle(const Vector3& v0, const Vector3& v1, const Vector3& v2, const Vector3& n)
: GeometricObject(RGBColor::RED),
a(v0), b(v1), c(v2)
{
    Vector3 ba = b - a;
    Vector3 ca = c - a;
    normal = ba.Cross(ca);
    normal = n;
}

Triangle::~Triangle() {}

bool Triangle::Hit( Ray ray, double& distance, ShadeRec shadeRec ) const
{
    //pierwsza macierz
    double A = a.x - b.x;
    double B = a.x - c.x;
    double C = ray.direction.x;
    double D = a.x - ray.origin.x;

    double E = a.y - b.y;
    double F = a.y - c.y;
    double G = ray.direction.y;
    double H = a.y - ray.origin.y;

    double I = a.z - b.z;
    double J = a.z - c.z;
    double K = ray.direction.z;
    double L = a.z - ray.origin.z;

    //dzia�ania z r�wna� na beta, gamma, t
    double M = F * K - G * J;
    double N = H * K - G * L;
    double P = F * L - H * J;
    double Q = G * I - E * K;
    double S = E * J - F * I;

    double invDenominator = 1.0 / (A * M + B * Q + C * S);

    double e1 = D * M - B * N - C * P;
    double beta = e1 * invDenominator;

    if(beta < 0.0)
    {
        shadeRec.hitAnObject = false;
        return false;
    }

    double R = E * L - H * I;
    double e2 = A * N + D * Q + C * R;
    double gamma = e2 * invDenominator;

    if(gamma < 0.0)
    {
        shadeRec.hitAnObject = false;
        return false;
    }
    if(beta + gamma > 1.0)
    {
        shadeRec.hitAnObject = false;
        return false;
    }

    double e3 = A * P - B * R + D * S;
    double t = e3 * invDenominator;

    double kEpsilon = 0.000001;

    if( t < kEpsilon)
    {
        shadeRec.hitAnObject = false;
        return false;
    }

    distance = t;
    shadeRec.normal = normal;
    shadeRec.localHitPoint = ray.origin + ray.direction * t;
    shadeRec.hitAnObject = true;


    return true;
}

void Triangle::SetA(Vector3 v)
{
    a = v;
}

void Triangle::SetB(Vector3 v)
{
    b = v;
}

void Triangle::SetC(Vector3 v)
{
    c = v;
}

void Triangle::SetNormal(Vector3 v)
{
    normal = v;
}

Vector3 Triangle::GetA()
{
    return a;
}

Vector3 Triangle::GetB()
{
    return b;
}

Vector3 Triangle::GetC()
{
    return c;
}

Vector3 Triangle::GetNormal()
{
    return normal;
}

void Triangle::Display()
{
    //------------
    a.Display();
    b.Display();
    c.Display();
    //-----------
}
