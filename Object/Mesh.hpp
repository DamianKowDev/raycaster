#pragma once

#include "GeometricObject.hpp"
#include "Triangle.hpp"
#include <vector>
#include <string>
#include "../Renderer/World.hpp"


class Mesh
{
private:

public:
    std::vector<Triangle*> triangles;

    void AddObjectsToWorld(World& world);


};
