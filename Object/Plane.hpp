#pragma once
#ifndef _PLANE_
#define _PLANE_

#include "../Utility/Vector3.hpp"
#include "../Renderer/Ray.hpp"
#include "../Utility/ShadeRec.hpp"
#include "../Object/GeometricObject.hpp"
#include "../Utility/RGBColor.hpp"

class Plane : public GeometricObject
{

public:
    Vector3 position;
    Vector3 normal;

    Plane();
    Plane(Vector3 &pos, Vector3 &normal);
     Plane(Vector3 &pos, Vector3 &normal, RGBColor color);
    ~Plane();

    virtual bool Hit( Ray ray, double& distance, ShadeRec shadeRec) const;


};



#endif // _PLANE_
