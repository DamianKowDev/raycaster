#include "GeometricObject.hpp"

GeometricObject::GeometricObject(RGBColor color)
    :color(color) {}

RGBColor GeometricObject::GetColor()
{
    return this->color;
}

void GeometricObject::SetColor(RGBColor color)
{
    this->color = color;
}
