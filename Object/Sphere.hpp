#pragma once
#ifndef _SPHERE_
#define _SPHERE_

#include "../Utility/Vector3.hpp"
#include "../Renderer/Ray.hpp"
#include "../Object/GeometricObject.hpp"
#include "../Utility/ShadeRec.hpp"

class Sphere : public GeometricObject
{
private:
    float radius;
    Vector3 center;
public:

    Sphere();
    Sphere(Vector3 &center, float radius);
    Sphere(Vector3 &center, float radius, RGBColor color);
    ~Sphere();

    void SetRadius(float radius); //czy przez &????
    float GetRadius();
    void SetCenter(Vector3 &center);
    Vector3 GetCenter();

    int Intersect(Ray &ray, float &distance);
    Vector3 GetNormal(const Vector3 &pi) const;

    virtual bool Hit( Ray ray, double& distance, ShadeRec shadeRec) const;

};




#endif // _SPHERE_
