#include "Mesh.hpp"
#include <iostream>
#include "../Utility/OBJLoader.hpp"

void Mesh::AddObjectsToWorld(World& world)
{
    for(int i = 0; i < triangles.size(); i++)
    {
        world.AddObject(triangles[i]);
    }
}
