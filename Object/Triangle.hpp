#pragma once
#include "GeometricObject.hpp"
#include "../Utility/Vector3.hpp"
#include "../Renderer/Ray.hpp"
#include "../Utility/ShadeRec.hpp"
#include "../Utility/RGBColor.hpp"

class Triangle : public GeometricObject
{

private:
    Vector3 a,b,c;
    Vector3 normal;

public:
    Triangle();
    Triangle(const Vector3& v0, const Vector3& v1, const Vector3& v2);
    Triangle(const Vector3& v0, const Vector3& v1, const Vector3& v2, const Vector3& n);
    ~Triangle();
    virtual bool Hit( Ray ray, double& distance, ShadeRec shadeRec) const;

    Vector3 GetA();
    Vector3 GetB();
    Vector3 GetC();
    Vector3 GetNormal();

    void SetA(Vector3 v);
    void SetB(Vector3 v);
    void SetC(Vector3 v);
    void SetNormal(Vector3 v);

    void Display();


};
