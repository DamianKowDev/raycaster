#include "Sphere.hpp"

Sphere::Sphere()
    :GeometricObject(RGBColor::RED)
{
    this->radius = 1.0f;
    this->center = Vector3::zero;
}

Sphere::Sphere(Vector3 &center, float radius)
    :GeometricObject(RGBColor::RED)
{
    this->radius = radius;
    this->center = center;
}

Sphere::Sphere(Vector3 &center, float radius, RGBColor color)
    :GeometricObject(color)
{
    this->radius = radius;
    this->center = center;
}

Sphere::~Sphere(){}

float Sphere::GetRadius()
{
    return this->radius;
}

Vector3 Sphere::GetCenter()
{
    return this->center;
}

void Sphere::SetRadius(float radius)
{
    if(radius > 0)
        this->radius = radius;
}

void Sphere::SetCenter(Vector3 &center)
{
    this->center = center;
}

int Sphere::Intersect(Ray &ray, float &distance)
{
    Vector3 v = ray.GetOrigin() - this->center;
    float b = v.Dot(ray.GetDirection()) * -1;
    float det = (b*b) - v.Dot(v) + this->radius;
    int retVal = -1; //MISS
    if(det > 0)
    {
        det = sqrt(det);
        float i1 = b - det;
        float i2 = b + det;
        if(i2 > 0)
        {
            if(i1 < 0)
            {
                if(i2 < distance)
                {
                    distance = i2;
                    retVal = 0; //INPRIM
                }
            }
            else
            {
                if(i1 < distance)
                {
                    distance = i1;
                    retVal = 1; //HIT
                }
            }
        }
    }

    return retVal;
}

Vector3 Sphere::GetNormal(const Vector3 &pi) const
{
    return (pi - this->center) / this->radius;
}

bool Sphere::Hit( Ray ray, double& distance, ShadeRec shadeRec) const  //TODO referancja dla shadeRec
{
    double t;
    Vector3 tmp = ray.origin - this->center;//ray.GetOrigin() - center;
    double a = (ray.direction).Dot(ray.direction);
    double b = 2.0 * tmp.Dot(ray.direction);
    double c = tmp.Dot(tmp) - this->radius * this->radius;
    double disc = b * b - 4.0 * a * c;

    double kEpsilon = 0.000001;

    if(disc < 0.0)
    {
        shadeRec.hitAnObject = false;
        return false;
    }
    else
    {
        double e = sqrt(disc);
        double denom = 2.0 * a;
        t = (- b - e) / denom;

        if(t > kEpsilon)
        {
            distance = t;
            shadeRec.normal = (tmp +  ray.direction * t) / radius;
            shadeRec.localHitPoint = ray.origin + ray.direction * t;
            shadeRec.hitAnObject = true;
            return true;
        }

        t = (-b + e) / denom;

        if(t > kEpsilon)
        {
            distance = t;
            shadeRec.normal = (tmp + ray.direction * t) /radius;
            shadeRec.localHitPoint = ray.origin + ray.direction * t;
            shadeRec.hitAnObject = true;
            return true;
        }

    }
    shadeRec.hitAnObject = false;
    return false;
}
