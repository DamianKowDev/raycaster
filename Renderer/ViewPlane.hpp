#pragma once
#ifndef _VIEWPLANE_
#define _VIEWPLANE_

#include "../Sampler/Sampler.hpp"
#include "../Utility/RGBColor.hpp"


class ViewPlane
{
public:
    int hRes;
    int vRes;
    float pixelSize;
    float gamma;
    float invGamma;
    int numSamples;
    Sampler* samplerPtr;
    RGBColor** pixelArray;

    ViewPlane();
    ViewPlane(const ViewPlane& vp);
    //~ViewPlane();

    void SetHorizontalRes(int horizontalRes);
    void SetVerticalRes(int verticalRes);
    void SetPixelSize(float pixelSize);
    void SetGamma(float gamma);
    void SetNumSamples(int numSamples);
    void SetSampler(Sampler* sp);
    //void SetSamples(const int n);

};



#endif // _VIEWPLANE_
