#include "../Renderer/World.hpp"
//#include "Sphere.hpp"
#include "../Renderer/Ray.hpp"
#include "../Utility/Vector3.hpp"
#include <ostream>
#include <fstream>
#include "../Renderer/Tracer.hpp"
#include "../Renderer/SingleSphere.hpp"
#include "../Renderer/MultipleObjects.hpp"
#include "../Object/Plane.hpp"
#include "../Camera/PinholeCamera.hpp"
#include "../Sampler/Jittered.hpp"
#include "../Camera/OrtographicCamera.hpp"
#include "../Object/Triangle.hpp"
#include "../Object/Mesh.hpp"
#include "../Utility/OBJParser.hpp"

void World::Build()
{
   int numSamples = 9; //9, dla 16 troche gorzej, n * n
   //Sampler* sampler = new Jittered(numSamples);
   viewPlane.SetSampler(new Jittered(numSamples));

    viewPlane.SetHorizontalRes(700);
    viewPlane.SetVerticalRes(700);
    viewPlane.SetPixelSize(0.2); //0.2
    viewPlane.SetGamma(1.0);

    viewPlane.pixelArray = new RGBColor*[viewPlane.vRes];
    for(int i = 0; i < viewPlane.vRes; i++)
    {
        viewPlane.pixelArray[i] = new RGBColor[viewPlane.hRes];
    }

    backgroundColor = RGBColor::LIGHT_SKY_BLUE;
    tracerPtr = new MultipleObjects(this);

    Sphere* spherePtr = new Sphere;
    Vector3 center(20.0,20.0,0.0);
    spherePtr->SetCenter(center);
    spherePtr->SetRadius(15.0);
    spherePtr->SetColor(RGBColor::ORANGE);
    this->AddObject(spherePtr);

    OBJParser parser;
    parser.LoadRawModel("ico.obj");
    Mesh mesh;
    mesh.triangles = parser.triangles;
    mesh.AddObjectsToWorld(*this);
    std::cout << "Wczytano trojkaty" << std::endl;


   PinholeCamera* pinholePtr = new PinholeCamera();
   pinholePtr->eye = Vector3(0, 0, 80.0);
   pinholePtr->lookAt = Vector3(0,0,-1);
   pinholePtr->distance = 50;
   pinholePtr->ComputeUVW();
   cameraPtr = pinholePtr;

   //OrtographicCamera* ortPtr = new OrtographicCamera();
   //cameraPtr = ortPtr;
}


#include <iostream>
void World::RenderScene()
{
    RGBColor pixelColor;
    Ray ray;
    double zw = 100.0;
    double x, y;

    ray.direction = Vector3(0,0,-1);

    constexpr const char *FILE_NAME = "out.ppm";
    std::ofstream out(FILE_NAME);
    out << "P3\n" << viewPlane.hRes << '\n' << viewPlane.vRes << '\n' << "255\n";
    std::cout << "Render start" << std::endl;
    for(int r = 0; r < viewPlane.vRes; r++)
    {
       for(int c = 0; c < viewPlane.hRes; c++)
       {
            x = viewPlane.pixelSize * (c - 0.5 * (viewPlane.hRes - 1.0));
            y = viewPlane.pixelSize * (r - 0.5 * (viewPlane.vRes - 1.0));
            ray.origin = Vector3(x,y,zw); //x,y | c,r
            pixelColor = tracerPtr->TraceRay(ray);
           //World tmp(*this);
           //ShadeRec sr(tmp);
           //float tmpFloat = ray.origin.z;

           //if(sphere.Hit(ray, tmpFloat, sr))
           //{
            //    pixelColor = RGBColor::RED;
               // std::cout << "Intersecting " << r << " " << c << std::endl;
           //}
           //else
           //{
           //    pixelColor = RGBColor::BLACK;
           //}
           //this->DisplayPixel(r,c, pixelColor);
           out << pixelColor;
       }
    }
    std::cout << "Koniec renderingu" << std::endl;
}



void World::DisplayPixel(const int row, const int column, const RGBColor &pixelColor)
{
    //out << pixelColor;
}

ShadeRec World::HitBareBonesObjects(Ray ray)
{
    ShadeRec sr(*this);
    double t;
    double tMin = 99999999; //dystans renderingu 99999999
    int numObjects = objects.size();

    for(int j = 0; j < numObjects; j++)
    {
        if(objects[j]->Hit(ray, t, sr) && (t < tMin))
        {
            sr.hitAnObject = true;
            tMin = t;
            sr.color = objects[j]->GetColor();
        }
    }
    return sr;
}

void World::RenderPerspectiveScene()
{
    RGBColor pixelColor;
    Ray ray;
    double zw = 100.0;
    double x, y;

    ray.direction = Vector3(0,0,-1);

    constexpr const char *FILE_NAME = "out.ppm";
    std::ofstream out(FILE_NAME);
    out << "P3\n" << viewPlane.hRes << '\n' << viewPlane.vRes << '\n' << "255\n";
    std::cout << "Render start" << std::endl;

    float eye = 95.0;
    float dist = -50;
    ray.origin = Vector3(0.0,0.0,eye);
    for(int c = 0; c < viewPlane.vRes; c++)
    {
       for(int r = 0; r < viewPlane.hRes; r++)
       {
           // x = viewPlane.pixelSize * (c - 0.5 * (viewPlane.hRes - 1.0));
          // y = viewPlane.pixelSize * (r - 0.5 * (viewPlane.vRes - 1.0));
            ray.direction = Vector3(viewPlane.pixelSize * (c - 0.5 *(viewPlane.hRes - 1.0)),viewPlane.pixelSize * (r - 0.5 *(viewPlane.vRes - 1.0)), dist); //x,y
            ray.direction = ray.direction.Normalize();
            pixelColor = tracerPtr->TraceRay(ray);
           //World tmp(*this);
           //ShadeRec sr(tmp);
           //float tmpFloat = ray.origin.z;

           //if(sphere.Hit(ray, tmpFloat, sr))
           //{
            //    pixelColor = RGBColor::RED;
               // std::cout << "Intersecting " << r << " " << c << std::endl;
           //}
           //else
           //{
           //    pixelColor = RGBColor::BLACK;
           //}
           //this->DisplayPixel(r,c, pixelColor);
           out << pixelColor;
       }
    }
    std::cout << "Koniec renderingu perspektywicznego" << std::endl;
}
