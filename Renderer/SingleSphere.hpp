#pragma once
#ifndef _SINGLESPHERE_
#define _SINGLESPHERE_

#include "Tracer.hpp"

class SingleSphere : public Tracer
{
public:
    SingleSphere(World* world);
    virtual RGBColor TraceRay(Ray ray);
};

#endif // _SINGLESPHERE_
