#pragma once
#ifndef _WORLD_
#define _WORLD_

#include <vector>

#include "../Renderer/ViewPlane.hpp"
#include "../Object/Sphere.hpp"
#include "../Utility/RGBColor.hpp"
#include "../Renderer/Ray.hpp"
#include "../Object/GeometricObject.hpp"
#include "../Camera/Camera.hpp"

class Tracer;

class World
{

public:
    Sphere sphere;
    ViewPlane viewPlane;
    RGBColor backgroundColor;
    Tracer* tracerPtr;
    Camera* cameraPtr;
    std::vector<GeometricObject*> objects;

   // World();
    void Build();
    void RenderScene();
    void RenderPerspectiveScene();
    void AddObject(GeometricObject* objectPtr);
    ShadeRec HitBareBonesObjects(Ray ray);
    void DisplayPixel(const int row, const int column, const RGBColor &pixelColor);
};

inline void World::AddObject(GeometricObject* objectPtr)
{
    objects.push_back(objectPtr);
}

#endif // _WORLD_
