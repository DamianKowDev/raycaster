#include "MultipleObjects.hpp"
#include <iostream>


MultipleObjects::MultipleObjects(World* worldPtr)
    :Tracer(worldPtr){}

RGBColor MultipleObjects::TraceRay(Ray ray)
{
    ShadeRec sr(worldPtr->HitBareBonesObjects(ray));

    //std::cout << "czy hit " << sr.hitAnObject << std::endl;

    if(sr.hitAnObject)
        return sr.color;
    else
        return worldPtr->backgroundColor;


}
