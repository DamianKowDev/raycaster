#include "ViewPlane.hpp"
#include <iostream>

ViewPlane::ViewPlane()
{

}

ViewPlane::ViewPlane(const ViewPlane& vp)
{
    this->hRes = vp.hRes;
    this->vRes = vp.vRes;
    this->pixelSize = vp.pixelSize;
    this->numSamples = vp.numSamples;
}

void ViewPlane::SetHorizontalRes(int horizontalRes)
{
    if(horizontalRes <= 0)
        return;
    this->hRes = horizontalRes;
}
void ViewPlane::SetVerticalRes(int verticalRes)
{
      if(verticalRes <= 0)
        return;
    this->vRes = verticalRes;
}
void ViewPlane::SetPixelSize(float pixelSize)
{
    if(pixelSize <= 0.0)
        return;
    this->pixelSize = pixelSize;
}
void ViewPlane::SetGamma(float gamma)
{
    this->gamma = gamma;
}

void ViewPlane::SetNumSamples(int numSamples)
{
    this->numSamples = numSamples;
}

void ViewPlane::SetSampler(Sampler* sp)
{
   /* if(samplerPtr)
    {
        delete samplerPtr;
        samplerPtr = NULL;
    }*/
    numSamples = sp->GetNumSamples();
    samplerPtr = sp;
    //std::cout << "Sampler zainicjowany" << std::endl;
}
