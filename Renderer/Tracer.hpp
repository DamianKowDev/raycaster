#pragma once
#ifndef _TRACER_
#define _TRACER_

#include "../Utility/RGBColor.hpp"
#include "../Renderer/World.hpp"

//class World;

class Tracer
{

public:
    Tracer();
    Tracer(World* worldPtr);

    virtual RGBColor TraceRay(Ray ray);

protected:
    World *worldPtr;

};


#endif // _TRACER_
