#pragma once
#include "Tracer.hpp"

class MultipleObjects : public Tracer
{
public:
    MultipleObjects(World* world);
    virtual RGBColor TraceRay(Ray ray);

};
