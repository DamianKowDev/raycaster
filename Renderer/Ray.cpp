#include "Ray.hpp"

Ray::Ray()
{
    origin = Vector3::zero;
    direction = Vector3::zero;
    distance = 0.0f;
}

Ray::Ray(Vector3 origin, Vector3 direction)
{
    this->origin = origin;
    this->direction = direction;
}

Ray::~Ray(){}

Vector3 Ray::GetOrigin()
{
    return this->origin;
}

Vector3 Ray::GetDirection()
{
    return this->direction;
}
