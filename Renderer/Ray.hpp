#pragma once
#ifndef _RAY_
#define _RAY_
#include "../Utility/Vector3.hpp"


class Ray
{
private:

public:
    Vector3 origin;
    Vector3 direction;
    Vector3 destination;
    float distance;

    Ray();
    Ray(Vector3 origin, Vector3 direction);
    ~Ray();

    Vector3 GetOrigin();
    Vector3 GetDirection();

};


#endif // _RAY_
