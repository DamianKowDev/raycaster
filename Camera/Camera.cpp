#include "Camera.hpp"
#include <iostream>

void Camera::ComputeUVW()
{
    std::cout << "ComputeUVW" << std::endl;
    this->w = eye - lookAt;
    this->w = this->w.Normalize();
    this->u = this->up.Cross(this->w);
    this->u = this->u.Normalize();
    this->v = this->w.Cross(this->u);
}
