#include "OrtographicCamera.hpp"
#include "../Renderer/World.hpp"
#include "../Renderer/Tracer.hpp"
#include <fstream>
#include <iostream>

OrtographicCamera::OrtographicCamera()
{

}

void OrtographicCamera::RenderScene(World& world)
{
    RGBColor pixelColor;
    Ray ray;
    double zw = 100.0;
    double x, y;

    Vector2 sp;
    Vector2 pp;

    ray.direction = Vector3(0,0,-1);

    constexpr const char *FILE_NAME = "out.ppm";
    std::ofstream out(FILE_NAME);
    out << "P3\n" << world.viewPlane.hRes << '\n' << world.viewPlane.vRes << '\n' << "255\n";
    std::cout << "Render start" << std::endl;
    for(int r = 0; r < world.viewPlane.vRes; r++)
    {
       for(int c = 0; c < world.viewPlane.hRes; c++)
       {

        pixelColor = RGBColor::BLACK;

        for(int j = 0; j < world.viewPlane.numSamples; j++)
        {
            sp = world.viewPlane.samplerPtr->SampleUnitSquare();
            pp.x = world.viewPlane.pixelSize * (c - 0.5 * world.viewPlane.hRes + sp.x);

            //zaczynamy od �wiartki II, tak to -60 i -60, wi�c y odwr�cone
            pp.y = world.viewPlane.pixelSize * (r - 0.5 * world.viewPlane.vRes + sp.y);
            pp.y = pp.y * -1;

            ray.origin = Vector3(pp.x, pp.y, zw);
            pixelColor = pixelColor + world.tracerPtr->TraceRay(ray);
        }

        pixelColor = pixelColor / world.viewPlane.numSamples;
        out << pixelColor;
       }
    }
    std::cout << "Koniec renderingu - kamera ortogonalna" << std::endl;
}
