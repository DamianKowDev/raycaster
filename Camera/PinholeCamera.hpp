#pragma once

#include "../Camera/Camera.hpp"
#include "../Utility/Vector2.hpp"

class World;

class PinholeCamera : public Camera
{
public:

    PinholeCamera();

    Vector3 RayDirection(Vector2 point);
    virtual void RenderScene(World& world);



};
