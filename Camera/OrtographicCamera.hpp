#pragma once

#include "Camera.hpp"

class World;

class OrtographicCamera : public Camera
{
public:

    OrtographicCamera();
    virtual void RenderScene(World& world);



};
