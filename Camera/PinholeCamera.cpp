#include "PinholeCamera.hpp"
#include "../Renderer/World.hpp"
#include "../Renderer/Tracer.hpp"
#include <fstream>
#include <iostream>

PinholeCamera::PinholeCamera()
{
    eye = Vector3::UNIT_VECTOR;
    lookAt = Vector3::UNIT_VECTOR;
    //up = Vector3::UNIT_VECTOR;
    up = Vector3(0,-1,0);  //odwrotne osie?
    u = Vector3::UNIT_VECTOR;
    v = Vector3::UNIT_VECTOR;
    w = Vector3::UNIT_VECTOR;
    exposureTime = 0.0;
    distance = -800.0;
    zoom = 1.0;
}

void PinholeCamera::RenderScene(World& world)
{
    RGBColor pixelColor;
    ViewPlane vp(world.viewPlane);
    Ray ray;
    int depth = 0;
    Vector2 sp;
    Vector2 pp;

    //vp.pixelSize /= zoom;
    ray.origin = eye;

    std::ofstream out("out.ppm");
    out << "P3\n" << vp.hRes << '\n' << vp.vRes << '\n' << "255\n";
    std::cout << "Render start - pinhole camera objects = " << world.objects.size() << std::endl;

    for(int r = 0; r < vp.vRes; r++)
    {
        for(int c = 0; c < vp.hRes; c++)
        {
           /* float x = vp.pixelSize * (c - 0.5 * (vp.hRes - 1.0));
            float y = vp.pixelSize * (r - 0.5 * (vp.vRes - 1.0));
            Vector2 dir1(x, y);
            ray.direction = this->RayDirection(dir1);
            pixelColor = world.tracerPtr->TraceRay(ray);
            out << pixelColor;*/
            pixelColor = RGBColor::BLACK;
          //  std::cout << "numsamples = " << vp.numSamples << std::endl;
            for(int j = 0; j < vp.numSamples; j++)
            {
                    Vector2 pp;
                    Vector2 sp;
                    //std::cout << "Jestem przed sample unit square" << std::endl;
                    sp = world.viewPlane.samplerPtr->SampleUnitSquare();
                    //std::cout << "Wykonalem sample unit square" << std::endl;
                    pp.x = vp.pixelSize * (c - 0.5 * vp.hRes + sp.x);
                    pp.y = vp.pixelSize * (r - 0.5 * vp.vRes + sp.y);
                    pp.x = pp.x * -1; //czemu teraz na x, ort na y
                    ray.direction = this->RayDirection(pp);

                    pixelColor = pixelColor + world.tracerPtr->TraceRay(ray);
            }


            pixelColor = pixelColor / vp.numSamples;
            out << pixelColor;
        }
    }

    std::cout << "Koniec renderingu - pinhole camera" << std::endl;
}

Vector3 PinholeCamera::RayDirection(Vector2 point)
{
    Vector3 dir = this->u * point.x + this->v * point.y - this->w * distance;
    dir = dir.Normalize();
    return dir;
}
