#pragma once

#include "../Utility/Vector3.hpp"

class World;

class Camera
{
public:
    Vector3 eye;
    Vector3 lookAt;
    Vector3 up;
    Vector3 u, v, w;
    float exposureTime;
    float distance;
    float zoom;

    void ComputeUVW();
    virtual void RenderScene(World& world) = 0;

};
