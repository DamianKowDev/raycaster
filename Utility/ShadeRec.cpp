#include "ShadeRec.hpp"

ShadeRec::ShadeRec(World &w) :
    hitAnObject(false),
    localHitPoint(),
    normal(),
    color(RGBColor::BLACK),
    world(w)
    {

    }

ShadeRec::~ShadeRec(){}
