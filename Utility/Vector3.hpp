#pragma once
#ifndef _VECTOR3_
#define _VECTOR3_

#include <iostream>
#include <cmath>

class Vector3
{
public:

    double x;
    double y;
    double z;

    Vector3();
    Vector3(double x, double y, double z);
    ~Vector3();

    Vector3 operator + (const Vector3 &vec) const;
    Vector3 operator - (const Vector3 &vec) const;
    Vector3 operator * (const double scallar) const;
    Vector3 operator / (const double scallar) const;

    double Magnitude() const;
    Vector3 Normalize() const;
    double Dot(const Vector3 &vec) const;
    Vector3 Cross(const Vector3 &vec) const;

    Vector3 Reflect(Vector3 normal); //testing
    Vector3 Lerp(Vector3 vec, double t);

    void Display();


    static Vector3 zero;
    static Vector3 UNIT_VECTOR;


};





#endif // _VECTOR3_
