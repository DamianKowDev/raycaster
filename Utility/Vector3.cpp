#include "Vector3.hpp"

Vector3::Vector3()
{
    this->x = 0.0f;
    this->y = 0.0f;
    this->z = 0.0f;
}

Vector3::Vector3(double x, double y, double z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

Vector3::~Vector3(){}

Vector3 Vector3::operator + (const Vector3 &vec) const
{
    return Vector3(this->x + vec.x, this->y + vec.y, this->z + vec.z);
}

Vector3 Vector3::operator - (const Vector3 &vec) const
{
    return Vector3(this->x - vec.x, this->y - vec.y, this->z - vec.z);
}

Vector3 Vector3::operator * (const double scallar) const
{
    return Vector3(this->x * scallar, this->y * scallar, this->z * scallar);
}

Vector3 Vector3::operator / (const double scallar) const
{
    return Vector3(this->x / scallar, this->y / scallar, this->z / scallar);
}

double Vector3::Magnitude() const
{
    return sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
}

Vector3 Vector3::Normalize() const
{
    double magnitude = this->Magnitude();
    return Vector3(this->x / magnitude, this->y / magnitude, this->z / magnitude);
}

Vector3 Vector3::Cross(const Vector3 &vec) const
{
    double crossX = this->y*vec.z - this->z*vec.y;
    double crossY = this->z*vec.x - this->x*vec.z;
    double crossZ = this->x*vec.y - this->y*vec.x;
    return Vector3(crossX, crossY, crossZ);
}

double Vector3::Dot(const Vector3 &vec) const
{
    return this->x*vec.x + this->y*vec.y + this->z*vec.z;
}

Vector3 Vector3::Reflect(Vector3 normal)
{
    return *this - (normal * (double)(2 * this->Dot(normal)));
}

Vector3 Vector3::Lerp(Vector3 vec, double t)
{
    Vector3 tmpVec;
    tmpVec.x = this->x + t * (vec.x - this->x);
    tmpVec.y = this->y + t * (vec.y - this->y);
    tmpVec.z = this->z + t * (vec.z - this->z);
    return tmpVec;

}

void Vector3::Display()
{
    std::cout << this->x << " | " << this->y << " | " << this->z << std::endl;
}


Vector3 Vector3::zero = Vector3();
Vector3 Vector3::UNIT_VECTOR = Vector3(1,1,1);
