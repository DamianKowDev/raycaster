#pragma once
#ifndef _OBJLoader_
#define _OBJLoader_

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "Vector2.hpp"
#include "Vector3.hpp"

class OBJLoader
{

public:

    double *normalsArray;
    double *texturesArray;
    double *verticesArray;
    int *indicesArray;
    int verticesCount;

    OBJLoader();
    bool LoadRawModel(std::string fileName);
    void ProcessVertex(std::vector<std::string> vertexData, std::vector<int> &indices, std::vector<Vector2> textures, std::vector<Vector3> normals, double *textureArray, double *normalsArray);
    void CleanUp();

};




#endif // _OBJLoader_
