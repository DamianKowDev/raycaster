#include "Vector2.hpp"


Vector2::Vector2()
{
    this->x = 0.0f;
    this->y = 0.0f;
}

Vector2::Vector2(double x, double y)
{
    this->x = x;
    this->y = y;
}

Vector2::~Vector2(){}

Vector2 Vector2::operator + (const Vector2 &vec) const
{
    return Vector2(this->x + vec.x, this->y + vec.y);
}

Vector2 Vector2::operator - (const Vector2 &vec) const
{
    return Vector2(this->x - vec.x, this->y - vec.y);
}

Vector2 Vector2::operator * (const double scallar) const
{
    return Vector2(this->x * scallar, this->y * scallar);
}

Vector2 Vector2::operator / (const double scallar) const
{
    return Vector2(this->x / scallar, this->y / scallar);
}

double Vector2::Magnitude() const
{
    return sqrt(this->x * this->x + this->y * this->y);
}

double Vector2::Dot(const Vector2 &vec) const
{
    return this->x * vec.x + this->y * vec.y;
}

Vector2 Vector2::Normalize() const
{
    double magnitude = this->Magnitude();
    return Vector2(this->x / magnitude, this->y / magnitude);
}

void Vector2::Display()
{
    std::cout << this->x << " " << this->y << std::endl;
}
