#pragma once
#ifndef _RGBCOLOR_
#define _RGBCOLOR_

#include <ostream>

class RGBColor
{

public:
    float r;
    float g;
    float b;
    static constexpr char OS_DELIMITER = '\n';

    RGBColor();
    RGBColor(float r, float g, float b);
    ~RGBColor();

    RGBColor operator + (const RGBColor &color) const;
    RGBColor operator / (const int &scallar) const;
    RGBColor operator / (const double &scallar) const;

    const static RGBColor BLACK;
    const static RGBColor RED;
    const static RGBColor GREEN;
    const static RGBColor BLUE;
    const static RGBColor INDIAN_RED;
    const static RGBColor LIGHT_PINK;
    const static RGBColor RASPBERRY;
    const static RGBColor MIDNIGHT_BLUE;
    const static RGBColor LIGHT_SKY_BLUE;
    const static RGBColor LIME_GREEN;
    const static RGBColor YELLOW;
    const static RGBColor TOMATO;
    const static RGBColor GRAY;
    const static RGBColor ORANGE;
    const static RGBColor PURPLE;
    const static RGBColor WHITE;

};

inline std::ostream& operator<<(std::ostream& os, const RGBColor& c)
{
    os	<< int(c.r) << c.OS_DELIMITER
        << int(c.g) << c.OS_DELIMITER
        << int(c.b) << c.OS_DELIMITER
    ;

    return os;
}



#endif // _RGBCOLOR_
