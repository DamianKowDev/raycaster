#include "RGBColor.hpp"


RGBColor::RGBColor()
{
    this->r = 0.0;
    this->g = 0.0;
    this->b = 0.0;
}

RGBColor::RGBColor(float r, float g, float b)
{
    this->r = r;
    this->g = g;
    this->b = b;
}


RGBColor RGBColor::operator + (const RGBColor &color) const
{
    return RGBColor(r + color.r, g + color.g, b + color.b);
}

RGBColor RGBColor::operator / (const int &scallar) const
{
    return RGBColor(r / scallar, g / scallar, b / scallar);
}

RGBColor RGBColor::operator / (const double &scallar) const
{
    return RGBColor(r / scallar, g / scallar, b / scallar);
}

RGBColor::~RGBColor(){}

const RGBColor RGBColor::BLACK = RGBColor(0x00,0x00,0x00);
const RGBColor RGBColor::RED = RGBColor(0xFF,0x00,0x00);
const RGBColor RGBColor::GREEN = RGBColor(0x00,0xFF,0x00);
const RGBColor RGBColor::BLUE = RGBColor(0x00,0x00,0xFF);
const RGBColor RGBColor::ORANGE = RGBColor(0xFF, 0x9D,0x00);
const RGBColor RGBColor::PURPLE = RGBColor(0xC8,0x00,0xFF);
const RGBColor RGBColor::INDIAN_RED = RGBColor(0xB0,0x17,0x1F);
const RGBColor RGBColor::LIGHT_PINK = RGBColor(0xFF,0xB6,0xC1);
const RGBColor RGBColor::RASPBERRY = RGBColor(0x87,0x26,0x57);
const RGBColor RGBColor::MIDNIGHT_BLUE = RGBColor(0x19,0x19,0x70);
const RGBColor RGBColor::LIGHT_SKY_BLUE = RGBColor(0x87,0xCE,0xFA);
const RGBColor RGBColor::LIME_GREEN = RGBColor(0x32,0xCD,0x32);
const RGBColor RGBColor::YELLOW = RGBColor(0xEE,0xEE,0x00);
const RGBColor RGBColor::TOMATO = RGBColor(0xFF,0x63,0x47);
const RGBColor RGBColor::GRAY = RGBColor(0x84,0x84,0x84);
const RGBColor RGBColor::WHITE = RGBColor(0xFF,0xFF,0xFF);


