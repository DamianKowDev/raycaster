#pragma once
#ifndef _VECTOR2_
#define _VECTOR2_

#include <iostream>
#include <cmath>

class Vector2
{

public:

    double x;
    double y;

    Vector2();
    Vector2(double x, double y);
    ~Vector2();

    Vector2 operator + (const Vector2 &vec) const;
    Vector2 operator - (const Vector2 &vec) const;
    Vector2 operator * (const double scallar) const;
    Vector2 operator / (const double scallar) const;

    double Magnitude() const;
    Vector2 Normalize() const;
    double Dot(const Vector2 &vec) const;

    void Display();

};





#endif // _VECTOR2_
