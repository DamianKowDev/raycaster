#include "OBJLoader.hpp"

//#include "StringHelper.hpp"

OBJLoader::OBJLoader(){}

bool OBJLoader::LoadRawModel(std::string fileName)
{
/*
    if(fileName.substr(fileName.size() - 4, 4) != ".obj")
        return false;

    std::ifstream file(fileName.c_str());

    if(!file.is_open())
        return false;

    std::vector<Vector3> vertices;
    std::vector<Vector2> textures;
    std::vector<Vector3> normals;
    std::vector<int> indices;



    std::string currentLine;

    vertices.clear();
    textures.clear();
    normals.clear();
    indices.clear();

    while(std::getline(file, currentLine))
    {
        std::vector<std::string> splitted = Split(currentLine, ' ');
        if(StartsWith(currentLine, "v "))
        {
            Vector3 vertex(atof(splitted[1].c_str()), atof(splitted[2].c_str()), atof(splitted[3].c_str()));
            vertices.push_back(vertex);
        }
        else if(StartsWith(currentLine, "vn "))
        {
            Vector3 normal(atof(splitted[1].c_str()), atof(splitted[2].c_str()), atof(splitted[3].c_str()));
            normals.push_back(normal);
        }
        else if(StartsWith(currentLine, "vt "))
        {
            Vector2 texture(atof(splitted[1].c_str()), atof(splitted[2].c_str()));
            textures.push_back(texture);
        }
        else if(StartsWith(currentLine, "f "))
        {
            texturesArray = new double[vertices.size() * 2];    //wyciek
            normalsArray = new double[vertices.size() * 3];     //wyciek
            break;
        }
    }

    do
    {
        if(!StartsWith(currentLine, "f "))
            continue;

        std::vector<std::string> splitted = Split(currentLine, ' ');
        std::vector<std::string> vertex1 = Split(splitted[1], '/');
        std::vector<std::string> vertex2 = Split(splitted[2], '/');
        std::vector<std::string> vertex3 = Split(splitted[3], '/');

        //std::cout << "ProcessVertex " << indices[1] << std::endl;

        this->ProcessVertex(vertex1, indices, textures, normals, texturesArray, normalsArray);
        this->ProcessVertex(vertex2, indices, textures, normals, texturesArray, normalsArray);
        this->ProcessVertex(vertex3, indices, textures, normals, texturesArray, normalsArray);
    }
    while(std::getline(file, currentLine));

    verticesArray = new double[vertices.size() * 3]; // wyciek
    verticesCount = vertices.size() * 3;
    indicesArray = new int[indices.size()];

    for (size_t i = 0; i < vertices.size(); i++)
    {
        int index = (int) i;
        verticesArray[index*3] = vertices[index].x;
        verticesArray[index*3+1] = vertices[index].y;
        verticesArray[index*3+2] = vertices[index].z;
    }

    for(size_t i = 0; i < indices.size(); i++)
    {
        int index = (int) i;
        indicesArray[index] = indices[index];
    }

    return true;
    //loadToVAO(varticesArray, texturesArray, indicesArray)

  //  std::cout << "x = " << verticesArray[0] << " " << "y = " << verticesArray[1]<< " z = " << verticesArray[2] << " " << std::endl
   // << "texture x = " << texturesArray[80] << " texture y = " <<  texturesArray[81]
   // << "\nindices = " << indicesArray[0] << std::endl;
//
  //  for(size_t i = 0; i < textures.size(); i++)
   // {
  //      std::cout << "tx = " << textures[i].x << " ty = " << textures[i].y << std::endl;
  //  }*/
}

void OBJLoader::ProcessVertex(std::vector<std::string> vertexData, std::vector<int> &indices, std::vector<Vector2> textures, std::vector<Vector3> normals, double textureArray[], double normalsArray[])
{
    int currentVertexPointer = atoi(vertexData[0].c_str()) - 1;
   // std::cout << currentVertexPointer << std::endl;
    indices.push_back(currentVertexPointer);
   // Vector2 currentTex = textures[atoi(vertexData[1].c_str())-1];
   // textureArray[currentVertexPointer*2] = currentTex.x;
   // textureArray[currentVertexPointer*2+1] = 1 - currentTex.y; // czemu 1 - y???????????????
    Vector3 currentNorm = normals[atoi(vertexData[2].c_str()) - 1];
    normalsArray[currentVertexPointer*3] = currentNorm.x;
    normalsArray[currentVertexPointer*3+1] = currentNorm.y;
    normalsArray[currentVertexPointer*3+2] = currentNorm.z;
}

void OBJLoader::CleanUp()
{
    delete normalsArray;
    delete texturesArray;
    delete verticesArray;
    delete indicesArray;
}
