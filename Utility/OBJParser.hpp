#pragma once
#include <vector>
#include <string>
#include <fstream>

#include "../Object/Triangle.hpp"
#include "Vector2.hpp"
#include "Vector3.hpp"

class OBJParser
{
public:
    std::vector<Vector3> vertices;
    std::vector<Vector2> textures;
    std::vector<Vector3> indices;
    std::vector<Vector3> normals;
    std::vector<Triangle*> triangles;

    //OBJParser();
    bool LoadRawModel(std::string fileName);


};
