#include "OBJParser.hpp"
#include "StringHelper.hpp"
#include <iostream>

bool OBJParser::LoadRawModel(std::string fileName)
{
     if(fileName.substr(fileName.size() - 4, 4) != ".obj")
        return false;

    std::ifstream file(fileName.c_str());

    if(!file.is_open())
        return false;

    std::string currentLine;
    vertices.clear();
    textures.clear();
    indices.clear();
    normals.clear();
    triangles.clear();

     while(std::getline(file, currentLine))
    {
        std::vector<std::string> splitted = Split(currentLine, ' ');
        if(StartsWith(currentLine, "v "))
        {
            Vector3 vertex(atof(splitted[1].c_str()), atof(splitted[2].c_str()), atof(splitted[3].c_str()));
            vertices.push_back(vertex);
        }
        else if(StartsWith(currentLine, "vn "))
        {
            Vector3 normal(atof(splitted[1].c_str()), atof(splitted[2].c_str()), atof(splitted[3].c_str()));
            normals.push_back(normal);
        }
        else if(StartsWith(currentLine, "vt "))
        {
            Vector2 texture(atof(splitted[1].c_str()), atof(splitted[2].c_str()));
            textures.push_back(texture);
        }
        else if(StartsWith(currentLine, "f "))
        {
            std::vector<std::string> indices1 = Split(splitted[1], '/');
            std::vector<std::string> indices2 = Split(splitted[2], '/');
            std::vector<std::string> indices3 = Split(splitted[3], '/');

            Vector3 v1(atof(indices1[0].c_str()), atof(indices1[1].c_str()),0);// atof(indices1[2].c_str()) );
            Vector3 v2(atof(indices2[0].c_str()), atof(indices2[1].c_str()),0);// atof(indices2[2].c_str()) );
            Vector3 v3(atof(indices3[0].c_str()), atof(indices3[1].c_str()),0);// atof(indices3[2].c_str()) );

            Triangle* triangle = new Triangle(vertices[(int)v1.x-1],vertices[(int)v2.x-1],vertices[(int)v3.x-1] ); //wyciek
            triangles.push_back(triangle);

        }
    }


}
