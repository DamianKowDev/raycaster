#pragma once
#ifndef _SHADEREC_
#define _SHADEREC_

#include "Vector3.hpp"
#include "RGBColor.hpp"
//#include "World.hpp"

class World;

class ShadeRec
{
public:
    bool hitAnObject; // czy promien trafil w obiekt
    Vector3 localHitPoint;
    Vector3 normal;
    RGBColor color;
    World &world;

    ShadeRec(World &w);
    ~ShadeRec();
};


#endif // _SHADEREC_
