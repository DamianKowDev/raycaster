#include <random>
#include <time.h>

float RandomFloat(float a, float b)
{
    srand(time(NULL));
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}
