#include <iostream>
#include <conio.h>
/*#include "Vector3.hpp"
#include "Vector2.hpp"
#include "OBJLoader.hpp"
#include "Sphere.hpp"
#include "Ray.hpp"
#include "ShadeRec.hpp"
#include "Plane.hpp"*/

#include "Renderer/World.hpp"
#include "Utility/OBJParser.hpp"

using namespace std;

int main()
{
    //OBJLoader loader;

    //loader.LoadRawModel("stall.obj");
   /* Vector3 c(10,10,0);
    Sphere sphere(c, 3);

    Vector3 pos(0,4,0);
    Vector3 n(1,1,0);
    Plane plane(pos, n);

    World w;
    ShadeRec sr(w);

    Ray ray;
    ray.direction = Vector3(0,0,-1);


    cout << "start" << endl;
    for(int r = 0; r < 20; r++)
    {
        for(int c = 0; c < 20; c++)
        {
            ray.origin = Vector3(c, r, 100.0);
            float xd = 100.0;
            if(plane.Hit(ray, xd, sr))
            {
               // sr.localHitPoint.Display();
                cout << "@";
                //sr.localHitPoint.Display();
                //cout << "jupi" << endl;
            }
            else
             //   cout << "-";
            if(sr.hitAnObject)
            {
                sr.localHitPoint.Display();
            }

        }
        cout << endl;
    }

    cout << "stop" << endl;*/

    /*Vector3 origin(0,0,-20);
    Vector3 direction(0,1,0);
    Ray R2(origin, direction);
    Vector3 pos(0,0,0);
    Vector3 normal(0,1,1);
    Plane P(pos, normal);
    float distance = 10000.0;
    World w;
    ShadeRec sr(w);
    if(P.Hit(R2, distance, sr))
    {
        sr.localHitPoint.Display();
    }*/

    /*Vector3 center(0,0,0);
    Sphere S(center, 10.0);
    Vector3 origin(0,0,-20);
    Vector3 direction(0,0,1);
    Vector3 direction2(0,1,0);
    Ray R1(origin, direction);
    Ray R2(origin, direction2);
    float distance = 100.0;
    World w;
    ShadeRec sr(w);

    if(S.Hit(R1, distance, sr))
    {
        cout << "Ray 1 " << endl;
        sr.localHitPoint.Display();
    }

    if(S.Hit(R2, distance, sr))
    {
        cout << "Ray 2 " << endl;
        sr.localHitPoint.Display();
    }*/



    World world;
    world.Build();
    world.cameraPtr->RenderScene(world);

   // OBJParser parser;
    //parser.LoadRawModel("cube.obj");

    _getch();
    return 0;
}
